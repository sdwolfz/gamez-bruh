# It's Gamez, Bruh!

A gamez engine, bruh! Where I do whatever I want... cuz I can do whatever I
want... if... if you don't like it... go make your own ;)

## Development

### Requirements

| Name        | Link                                             |
|-------------|--------------------------------------------------|
| `bash`      | https://www.gnu.org/software/bash                |
| `docker`    | https://docs.docker.com/get-docker               |
| `envchain`  | https://github.com/sorah/envchain                |
| `make`      | https://www.gnu.org/software/make                |
| `xdg-utils` | https://freedesktop.org/wiki/Software/xdg-utils/ |

### Docker

Build docker image with:

```sh
make build
```

Start a shell using:

```sh
make shell
```

### NPM

Install packages:

```sh
npm install
```

// -----------------------------------------------------------------------------
// ESBuild
// -----------------------------------------------------------------------------

const esbuild = require('esbuild')
const version = require('./package.json')['version']

const targets = ['chrome58', 'firefox57', 'safari11']

// -----------------------------------------------------------------------------
// Minified

esbuild.build({
  entryPoints: ['src/main.js'],
  bundle: true,
  minify: true,
  sourcemap: true,
  target: targets,
  platform: 'browser',
  format: 'iife',
  outfile: 'dist/gamez-bruh.min.js',
  globalName: 'GamezBruh'
}).catch(() => process.exit(1))

esbuild.build({
  entryPoints: ['src/main.js'],
  bundle: true,
  minify: true,
  sourcemap: true,
  target: targets,
  platform: 'browser',
  format: 'iife',
  outfile: `dist/gamez-bruh.${version}.min.js`,
  globalName: 'GamezBruh'
}).catch(() => process.exit(1))

// -----------------------------------------------------------------------------
// Non-minified

esbuild.build({
  entryPoints: ['src/main.js'],
  bundle: true,
  minify: false,
  sourcemap: false,
  target: targets,
  platform: 'browser',
  format: 'iife',
  outfile: 'dist/gamez-bruh.js',
  globalName: 'GamezBruh'
}).catch(() => process.exit(1))

esbuild.build({
  entryPoints: ['src/main.js'],
  bundle: true,
  minify: false,
  sourcemap: false,
  target: targets,
  platform: 'browser',
  format: 'iife',
  outfile: `dist/gamez-bruh.${version}.js`,
  globalName: 'GamezBruh'
}).catch(() => process.exit(1))

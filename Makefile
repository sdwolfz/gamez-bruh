#-------------------------------------------------------------------------------
# Settings
#-------------------------------------------------------------------------------

# By default the `help` goal is executed.
.DEFAULT_GLOBAL := help

HERE := $(realpath $(dir $(realpath $(firstword $(MAKEFILE_LIST)))))
DOCKER_HERE := docker run --rm -it -u `id -u`:`id -g` -v $(HERE):/here -w /here
DOCKER_IMAGE_NAME := gamez-bruh

#-------------------------------------------------------------------------------
# Utility Goals
#-------------------------------------------------------------------------------

# # help
#
# Displays a help message containing usage instructions and a list of available
# goals.
#
# Usage:
#
# To display the help message run:
# ```bash
# make help
# ```
#
# This is also the default goal so you can run it as following:
# ```bash
# make
# ```
.PHONY: help
help:
	@echo 'Usage: make [<GOAL_1>, ...] [<VARIABLE_1>=value_1, ...]'
	@echo ''
	@echo 'Examples:'
	@echo '  make'
	@echo '  make help'
	@echo '  make css/webfonts'
	@echo ''
	@echo 'Utility goals:'
	@echo '  - help: Displays this help message.'
	@echo '  - open: Open the main webpage in a browser.'
	@echo 'Docker goals:'
	@echo '  - build: Build the development docker image.'
	@echo '  - shell: Run a shell with the development docker image.'
	@echo 'CI goals:'
	@echo '  - ci: Run all CI steps using docker'
	@echo '  - lint/eclint: Run `eclint`.'
	@echo '  - lint/hadolint: Run `hadolint`.'

# # open
#
# Open the main webpage in a browser.
#
# Usage:
#
# ```bash
# make open
# ```
.PHONY: open
open:
	@echo 'Opening: live/index.html'
	@xdg-open live/index.html

.PHONY: envchain
envchain:
	@envchain -s -n npm NPM_TOKEN

#-------------------------------------------------------------------------------
# Docker Goals
#-------------------------------------------------------------------------------

.PHONY: build
build:
	@docker build \
		--build-arg=HOST_USER_UID=`id -u` \
		--build-arg=HOST_USER_GID=`id -g` \
		-t $(DOCKER_IMAGE_NAME) .

.PHONY: shell
shell:
	@envchain npm $(DOCKER_HERE) -e NPM_TOKEN $(DOCKER_IMAGE_NAME) sh

#-------------------------------------------------------------------------------
# Node Goals
#-------------------------------------------------------------------------------

.PHONY: esbuild
esbuild:
	@node esbuild.js

.PHONY: shell/esbuild
shell/esbuild:
	@envchain npm $(DOCKER_HERE) -e NPM_TOKEN $(DOCKER_IMAGE_NAME) make esbuild

.PHONY: pack
pack:
	@npm pack --pack-destination=pkg

.PHONY: publish
publish:
	@npm config set //registry.npmjs.org/:_authToken ${NPM_TOKEN}
	@npm whoami
	@npm publish

#-------------------------------------------------------------------------------
# CI Goals
#-------------------------------------------------------------------------------

.PHONY: ci
ci:
	@$(DOCKER_HERE) sdwolfz/eclint:latest make lint/eclint
	@$(DOCKER_HERE) sdwolfz/hadolint:latest make lint/hadolint

.PHONY: lint/eclint
lint/eclint:
	@eclint check $(git ls-files)

.PHONY: lint/hadolint
lint/hadolint:
	@find . -name Dockerfile | xargs hadolint
